# i3lock-show-me-what-you-got

Bash script that adds a "Cromulon" head (from Rick and Morty's - Get Schwifty episode) in the middle of i3lock screen.

*it works atm in the primary monitor.*

## Installation

Just run the script.

## How it looks
![screenshot](screenshot.png)


## How it feels
![feels](feels.gif)
